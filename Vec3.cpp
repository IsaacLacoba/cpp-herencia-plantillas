// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "Vec3.h"

Vec3::Vec3(): _x(0), _y(0), _z(0) {}

Vec3::Vec3(const Vec3& vect){
  _x = vect.getX();
  _y = vect.getY();
  _z = vect.getZ();
}

Vec3::Vec3 (int x, int y, int z): _x(x), _y(y), _z(z){

}

Vec3::~Vec3 (){

}

void Vec3::operator+(const Vec3 &op) {
  _x += op.getX();
  _y += op.getY();
  _z += op.getZ();
}

void Vec3::operator- (const Vec3 &op){
  _x -= op.getX();
  _y -= op.getY();
  _z -= op.getZ();
}

int Vec3::getX () const{ return _x;}
int Vec3::getY () const{ return _y;}
int Vec3::getZ () const{ return _z;}

void Vec3::setX (int x) { _x = x;};
void Vec3::setY (int y) { _y = y;};
void Vec3::setZ (int z) { _z = z;};
