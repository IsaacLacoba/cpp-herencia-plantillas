// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#ifndef ENEMY_H
#define ENEMY_H

#include "entity3d.h"
#include "Vec3.h"
#include <string>

class Enemy:  public Entity3D {
  std::string action_;

 public:
  Enemy  (std::string name, Vec3 posicion, std::string action): Entity3D (name, posicion), action_(action){}

  std::string get_action () {
    return action_;
  }

  void set_action (std::string action) {
    action_ = action;
  }

  std::string to_string(){
    std::stringstream ss;
    ss << "(name: " << get_name() << ", action: " << action_ << ")";

    return ss.str();
  }
};

#endif
