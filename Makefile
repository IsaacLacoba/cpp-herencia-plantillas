CXX=g++ -std=c++11

CXXFLAGS = -ggdb -I$(HEADERDIR)

HEADERDIR=include

all: isaac-lacoba-s6-cedv2014

# isaac-lacoba-s6-cedv2014: $(TARGETS)

clean:
	$(RM)  $(TARGETS) isaac-lacoba-s6-cedv2014
