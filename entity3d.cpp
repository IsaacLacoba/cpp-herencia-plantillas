// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "entity3d.h"

Entity3D::Entity3D(std::string name, Vec3 position): name_(name), position_(position) {}

Vec3 Entity3D::get_position() {
    return position_;
}

std::string Entity3D::get_name() {
  return name_;
}

void Entity3D::set_position(Vec3 position) {
    position_ = position;
}

std::string Entity3D::to_string() {
  std::stringstream ss;;
  ss << "name: " << name_;
  return ss.str();
}
