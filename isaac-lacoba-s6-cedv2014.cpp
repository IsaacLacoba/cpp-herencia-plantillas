// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include <iostream>

#include "node.h"
#include "list.h"
#include "Vec3.h"

#include "entity3d.h"
#include "player.h"
#include "enemy.h"


class Test {
public:
  Test(){}
  ~Test(){}


  void test_node(){
    Node<int>* node = new Node<int>(1);
    Node<int>* node2 = new Node<int>(2);

    std::cout << node->get_elem() <<  std::endl;
    std::cout << node2->get_elem() << std::endl;

    node->set_next(node2);
    node->set_previous(node2);

    std::cout << node->get_next()->get_elem() << std::endl;
    std::cout << node->get_previous()->get_elem() << std::endl;

    std::cout << node->get_next() << std::endl;
    std::cout << node2 << std::endl;

    node2 = node;

    std::cout << node2->get_next() << std::endl;
    std::cout << node2->get_previous() << std::endl;

    std::cout << node2->get_next() << std::endl;
    std::cout << node << std::endl;
  }

  // void test_list() {
  //   List<int> list;

  //   list.insert_n(0, 1);

  //   list.insert_n(0, 2);

  //   list.insert_n(2, 3);

  //   list.insert_front(4);
  //   list.insert_back(5);

  //   list.print();
  //   list.insert_n(3, 9);
  //   list.print();

  //   list.delete_n(0);
  //   list.print();

  //   list.delete_n(list.get_size());
  //   list.print();

  //   list.delete_n(1);

  //   list.print();

  //   list.modify_n(0, 7);
  //   list.print();

  //   list.modify_n(list.get_size(), 4);
  //   list.print();

  //   list.modify_n(1, 6);
  //   list.print();
  // }

  void character_list(){
    List<Entity3D> list;

    Vec3 position;
    Entity3D entity("pochi", position);

    list.insert_front(entity);
    list.print();

    Vec3 enemy_position(1, 3, 5);
    Enemy enemy("calimaro", enemy_position, "atacar");

    list.insert_n(1, enemy);
    list.print();

    Vec3 ally_position(1, 3, 5);
    Player ally("Anabel", ally_position, "warrior");

    list.insert_back(ally);
    list.print();


  }

};

int main(int argc, char *argv[])
{
  Test().character_list();
  return 0;
}
