// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#ifndef PLAYER_H
#define PLAYER_H

#include "entity3d.h"
#include "Vec3.h"
#include <string>


class Player: public Entity3D {
  std::string type_;

 public:
  Player (std::string name, Vec3 position, std::string type): Entity3D (name, position), type_ (type) {}

  std::string get_type(){
    return type_;
  };

  void set_type(std::string type){
    type_ = type;
  }
};

#endif
