// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include <stdexcept>
#include "node.h"

#ifndef LISTA_H
#define LISTA_H

template<class T>
class List{
  Node<T>* head_;
  Node<T>* end_;

  int size;

  T get_n(int n);

 public:
  List(){
    size = 0;
    head_ = nullptr;
    end_ = nullptr;
  };

  ~List(){
    std::cout << __func__ << std::endl;
    Node<T> * current = head_;
    while (head_)  {
      head_ = head_->get_next();
      delete current;
      current = head_;
    }
  };

  Node<T>* front() {
    return head_;
  }

  Node<T>* back() {
    return end_;
  }

  int get_size() { return size;}

  void insert_n(int n,const T & new_elem){
    try{
      if(n > size || n < 0) throw std::out_of_range ("índice fuera de rango");
      if(size == 0){
        first_insert(new_elem);
        return;
      }

      if(n == 0) {
        insert_front(new_elem);

        return;
      }

      if(n == size){
        insert_back(new_elem);

        return;
      }

      insert_node(find_node(n), new_elem);

    }
    catch (const std::out_of_range& oor) {
      std::cerr << "Out of Range error: " << oor.what() << '\n';
    }


  }

  void insert_front(T new_elem){
     if(size == 0){
        first_insert(new_elem);
        return;
     }

    Node<T>* new_node = new Node<T>(new_elem);

    Node<T> * aux;
    aux = head_;
    head_ = new_node;

    head_->set_next(aux);
    aux->set_previous(head_);
    size++;

  }

  void insert_back(T new_elem) {
    Node<T>* new_node = new Node<T>(new_elem);

    Node<T> * aux;
    aux = end_;
    end_ = new_node;

    end_->set_previous(aux);
    aux->set_next(end_);

    size++;
  }

  void delete_n(int n) {
    try{
      if(n > size || n < 0) throw std::out_of_range ("indice fuera de rango");
      if(size == 0) return;

      if(n == 0){
        delete_front();
        return;
      }
      if(n == size){
        delete_back();
        return;
      }

      delete_node(find_node(n));
      size--;
    }
    catch (const std::out_of_range& oor) {
      std::cerr << "Out of Range error: " << oor.what() << '\n';
    }
  }

  void delete_front(){
    Node<T> * aux = head_;
    head_ = head_->get_next();
    head_->set_previous(nullptr);

    aux->set_next(nullptr);
    delete aux;
    size--;
  }

  void delete_back(){
    Node<T> * aux = end_;

    end_ = end_->get_previous();
    end_->set_next(nullptr);

    delete aux;

    size--;
  }



  void modify_n(int n, T new_value) {
    try{
      if(n > size || n < 0) throw std::out_of_range ("indice fuera de rango");

      find_node(n)->set_elem(new_value);
    }
    catch (const std::out_of_range& oor) {
      std::cerr << "Out of Range error: " << oor.what() << '\n';
    }

  }

  void print() {
    Node<T> * curr = head_;
    while (curr)
      {
        T t = curr->get_elem();

        std::cout << t.to_string() << " --> ";

        curr = curr->get_next();
      }
    std::cout << "NULL" << std::endl;
  }


 private:

  void first_insert(const T & new_elem){
    Node<T>* new_node = new Node<T>(new_elem);
    head_ = new_node;
    end_ = new_node;
    end_->set_previous(new_node);
    size++;
  }

  void insert_node(Node<T> *node, T new_elem){
    Node<T>* new_node = new Node<T>(new_elem);

    Node<T>* aux;
    aux = node->get_next();

    new_node->set_previous(node);
    new_node->set_next(aux);

    node->set_next(new_node);
    aux->set_previous(new_node);


    size++;
  }

  Node<T>* find_node(int n){
    int index = -1;
    for(Node<T> *aux = head_;
        aux != nullptr ;
        aux = aux->get_next(), index++)
      if (index == (n - 1)) { return aux;}

    if(n == size) return end_;

    return nullptr;
  }

  void delete_node(Node<T> * node){
    Node<T> * aux = node->get_next();
    node->set_next(aux->get_next());
    aux->get_next()->set_previous(node);

    delete aux;
  }
};

#endif
