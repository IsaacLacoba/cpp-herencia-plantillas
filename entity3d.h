// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#ifndef ENTITY3D_H
#define ENTITY3D_H

#include <iostream>
#include <sstream>
#include <string>
#include "Vec3.h"

class Entity3D {
  Vec3 position_;
  std::string name_;

 public:
  Entity3D(std::string name, Vec3 position);

  Vec3 get_position();

  std::string get_name();

  void set_position(Vec3 position);

  std::string to_string();
};

#endif
