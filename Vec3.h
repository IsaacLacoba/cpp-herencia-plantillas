#ifndef VEC3_H
#define VEC3_H
#include <iostream>


class Vec3 {
 public:
  Vec3();
  Vec3 (int x, int y, int z);
  Vec3(const Vec3& vect);
  ~Vec3 ();

  // Funciones de acceso.
  int getX () const;
  int getY () const;
  int getZ () const;

  void setX (int);
  void setY (int);
  void setZ (int);


  // Sobrecarga de operadores.
  void operator+ (const Vec3 &op);
  void operator- (const Vec3 &op);

 private:
  int _x, _y, _z; // Coordenadas 3D.
};

#endif
