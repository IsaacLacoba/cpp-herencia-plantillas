// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#ifndef NODE_H
#define NODE_H
#include <iostream>

template<class T>
class Node{
  Node<T> * previous_;
  Node<T> * next_;
  T elem_;

 public:
  Node<T>(const T& elem): elem_(elem), next_(nullptr){  }
  Node<T>(const Node<T>& node): elem_(node.get_elem()), next_(nullptr), previous_(nullptr){}
  ~Node<T>(){
    delete next_;
  };

  T get_elem() const{
    return elem_;
  }

  void set_elem(const int elem) {
    elem_ = elem;
  }

  Node<T>* get_next() const {
    return next_;
  }

  Node<T>* get_previous() const {
    return previous_;
  }

  void set_next(Node* next) {
    next_ = next;
  }

  void set_previous(Node* previous) {
    previous_ = previous;
  }

  Node& operator=(const Node& node) = default;
};

#endif
